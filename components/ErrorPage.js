
import {Container, Row, Col} from 'react-bootstrap'

export default function ErrorPage(){

	return(
		<Container className="my-5">
			<Row className="justify-content-center">
				<Col xs={10} md={8}>
					<div className="jumbotron">
						<h1>404 - Not Found</h1>
						<p>The Page you are looking for cannot be found</p>
						<p>Click here to go to <a href="/">Home<a/></p>
					</div>
				</Col>
			</Row>
		</Container>
	)
} 
